create table EXAMPLEFILTER_PARAMETER_VALUES (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    VAL_INT integer,
    VAL_STRING varchar(255),
    TYPE_PARAMETERS varchar(50),
    TYPE_ID uuid not null,
    PARAMETER_ID uuid not null,
    --
    primary key (ID)
);
