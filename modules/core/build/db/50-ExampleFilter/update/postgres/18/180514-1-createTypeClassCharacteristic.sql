create table EXAMPLEFILTER_TYPE_CLASS_CHARACTERISTIC (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    IS_MAIN boolean,
    PARAMETER_ID uuid not null,
    TYPE_CLASS_ID uuid not null,
    --
    primary key (ID)
);
