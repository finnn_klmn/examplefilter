-- alter table EXAMPLEFILTER_TYPE add column TYPE_CLASS_ID uuid ^
-- update EXAMPLEFILTER_TYPE set TYPE_CLASS_ID = <default_value> ;
-- alter table EXAMPLEFILTER_TYPE alter column TYPE_CLASS_ID set not null ;
alter table EXAMPLEFILTER_TYPE add column TYPE_CLASS_ID uuid not null ;
