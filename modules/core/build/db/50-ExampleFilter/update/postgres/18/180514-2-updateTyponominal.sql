-- alter table EXAMPLEFILTER_TYPONOMINAL add column TYPE_ID uuid ^
-- update EXAMPLEFILTER_TYPONOMINAL set TYPE_ID = <default_value> ;
-- alter table EXAMPLEFILTER_TYPONOMINAL alter column TYPE_ID set not null ;
alter table EXAMPLEFILTER_TYPONOMINAL add column TYPE_ID uuid not null ;
