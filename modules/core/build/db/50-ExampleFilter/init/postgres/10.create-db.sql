-- begin EXAMPLEFILTER_TYPONOMINAL
create table EXAMPLEFILTER_TYPONOMINAL (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    TYPE_ID uuid not null,
    --
    primary key (ID)
)^
-- end EXAMPLEFILTER_TYPONOMINAL
-- begin EXAMPLEFILTER_TYPE
create table EXAMPLEFILTER_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    TYPE_CLASS_ID uuid not null,
    --
    primary key (ID)
)^
-- end EXAMPLEFILTER_TYPE
-- begin EXAMPLEFILTER_TYPE_CLASS
create table EXAMPLEFILTER_TYPE_CLASS (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    PARENT_ID uuid,
    --
    primary key (ID)
)^
-- end EXAMPLEFILTER_TYPE_CLASS
-- begin EXAMPLEFILTER_PARAMETER_VALUES
create table EXAMPLEFILTER_PARAMETER_VALUES (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    VAL_INT integer,
    VAL_STRING varchar(255),
    TYPE_PARAMETERS varchar(50),
    TYPE_ID uuid not null,
    PARAMETER_ID uuid not null,
    --
    primary key (ID)
)^
-- end EXAMPLEFILTER_PARAMETER_VALUES
-- begin EXAMPLEFILTER_PARAMETER
create table EXAMPLEFILTER_PARAMETER (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    NAME varchar(255) not null,
    PARAMETER_TYPE varchar(255),
    --
    primary key (ID)
)^
-- end EXAMPLEFILTER_PARAMETER
-- begin EXAMPLEFILTER_TYPE_CLASS_CHARACTERISTIC
create table EXAMPLEFILTER_TYPE_CLASS_CHARACTERISTIC (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    IS_MAIN boolean,
    PARAMETER_ID uuid not null,
    TYPE_CLASS_ID uuid not null,
    --
    primary key (ID)
)^
-- end EXAMPLEFILTER_TYPE_CLASS_CHARACTERISTIC
