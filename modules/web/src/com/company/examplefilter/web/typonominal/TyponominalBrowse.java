package com.company.examplefilter.web.typonominal;

import com.company.examplefilter.entity.ParameterValueType;
import com.company.examplefilter.entity.ParameterValues;
import com.company.examplefilter.entity.Type;
import com.company.examplefilter.entity.TypeClassCharacteristic;
import com.haulmont.cuba.gui.components.*;
import com.haulmont.cuba.gui.components.filter.FilterDelegateImpl;
import com.haulmont.cuba.gui.data.CollectionDatasource;
import com.haulmont.cuba.gui.data.Datasource;
import com.haulmont.cuba.gui.data.GroupDatasource;
import com.haulmont.cuba.gui.data.HierarchicalDatasource;
import com.haulmont.cuba.gui.xml.layout.ComponentsFactory;
import com.haulmont.cuba.web.gui.components.WebFilter;

import javax.inject.Inject;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TyponominalBrowse extends AbstractLookup {
    //Datasources
    @Inject
    private GroupDatasource typonominalsDs;
    @Inject private CollectionDatasource typesDs;
    @Inject private HierarchicalDatasource typeClassesDs;

    @Inject private LookupField paramTypeLookupField;
    @Inject private ComponentsFactory componentsFactory;
    @Inject private Container parameterVbox;

    @Inject private FilterDelegateImpl typonominalFilter;

    @Override
    public void ready() {
        typeClassesDs.addItemChangeListener(new Datasource.ItemChangeListener() { //Выбор Класса ЭКБ
            @Override
            public void itemChanged(Datasource.ItemChangeEvent e) {
                if(e.getItem() != null) {
                    String queryTypeClass = "select e from examplefilter$Type e where e.typeClass.id = '" + e.getItem().getId() + "'";
                    typesDs.setQuery(queryTypeClass);
                    typesDs.refresh();
                    String queryType = "select e from examplefilter$Typonominal e where e.type.typeClass.id = '" + e.getItem().getId() + "'";
                    typonominalsDs.setQuery(queryType);
                    typonominalsDs.refresh();
                }
            }
        }); //Выбор Класса ЭКБ
        typesDs.addItemChangeListener(eType -> { //Выбор Типа ЭКБ
            if(eType.getItem() != null) {
                String queryType = "select e from examplefilter$Typonominal e where e.type.id = '" + eType.getItem().getId() + "'";
                typonominalsDs.setQuery(queryType);
                typonominalsDs.refresh();
            }
        }); //Выбор Типа ЭКБ

        typonominalsDs.addItemChangeListener(e -> {
            parameterVbox.removeAll();
            if (e.getItem() != null) {
                screenParameter(e.getItem().getValue("type"));
            }
        });

        paramTypeLookupField.addValueChangeListener(eTypeParam ->{ //Типы параметров
            parameterVbox.removeAll();
            if(typonominalsDs.getItem() != null) {
                screenParameter(typonominalsDs.getItem().getValue("type"));
            }
        }); //Типы параметров

        super.ready();
    }

    private void screenParameter(Type type){
        if(type.getParameterValue().size() > 0){
            Collection<ParameterValues> parameterValueCollection = type.getParameterValue();
            Collection<TypeClassCharacteristic> characteristicCollection = type.getTypeClass().getCharacteristic();
                for (ParameterValues parameterValue : parameterValueCollection) {
                    if (paramTypeLookupField.getValue() == null || parameterValue.getParameter().getParameterType() == paramTypeLookupField.getValue()) {
                        screenParameterDisplay(parameterValue);
                    }
                }
        }
    } //условия для формирования параметров

    private void screenParameterDisplay(ParameterValues parameterValue){

        HBoxLayout hBoxContainer = componentsFactory.createComponent(HBoxLayout.class);
        hBoxContainer.setId("hBoxContainerID" + parameterVbox.getId());
        hBoxContainer.setSpacing(true);
        hBoxContainer.setWidth("100%");

        Label parameterLbl = componentsFactory.createComponent(Label.class);
        parameterLbl.setId("parameterLblID" + hBoxContainer.getId());
        parameterLbl.setValue(parameterValue.getParameter().getName() + ": ");
        parameterLbl.setAlignment(Alignment.MIDDLE_LEFT);
        parameterLbl.setWidth("200px");
        hBoxContainer.add(parameterLbl);

        TextField parameterField = componentsFactory.createComponent(TextField.class);
        parameterField.setId("parameterFieldID" + hBoxContainer.getId());
        parameterField.setWidth("100%");
        parameterField.setEditable(false);
        hBoxContainer.add(parameterField);

        if (parameterValue.getTypeParameters() == ParameterValueType.integer) {
            parameterField.setValue(parameterValue.getValInt().toString());
        }

        if (parameterValue.getTypeParameters() == ParameterValueType.string) {
            parameterField.setValue(parameterValue.getValString());
        }

        parameterVbox.add(hBoxContainer);
    } //формирование визуального представления параметров
}