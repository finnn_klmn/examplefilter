package com.company.examplefilter.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import java.util.List;
import javax.persistence.OneToMany;

@NamePattern("%s|name")
@Table(name = "EXAMPLEFILTER_PARAMETER")
@Entity(name = "examplefilter$Parameter")
public class Parameter extends StandardEntity {
    private static final long serialVersionUID = -1118595530801582223L;

    @Column(name = "NAME", nullable = false)
    protected String name;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "parameter")
    protected List<ParameterValues> parameterValue;

    @Column(name = "PARAMETER_TYPE")
    protected String parameterType;

    public ParameterType getParameterType() {
        return parameterType == null ? null : ParameterType.fromId(parameterType);
    }

    public void setParameterType(ParameterType parameterType) {
        this.parameterType = parameterType == null ? null : parameterType.getId();
    }



    public void setParameterValue(List<ParameterValues> parameterValue) {
        this.parameterValue = parameterValue;
    }

    public List<ParameterValues> getParameterValue() {
        return parameterValue;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}