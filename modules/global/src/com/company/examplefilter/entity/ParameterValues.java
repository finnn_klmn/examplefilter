package com.company.examplefilter.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@NamePattern("%s|typeParameters")
@Table(name = "EXAMPLEFILTER_PARAMETER_VALUES")
@Entity(name = "examplefilter$ParameterValues")
public class ParameterValues extends StandardEntity {
    private static final long serialVersionUID = 4715025309873527137L;

    @Column(name = "VAL_INT")
    protected Integer valInt;

    @Column(name = "VAL_STRING")
    protected String valString;

    @Column(name = "TYPE_PARAMETERS")
    protected String typeParameters;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "TYPE_ID")
    protected Type type;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "PARAMETER_ID")
    protected Parameter parameter;

    public void setParameter(Parameter parameter) {
        this.parameter = parameter;
    }

    public Parameter getParameter() {
        return parameter;
    }


    public void setType(Type type) {
        this.type = type;
    }

    public Type getType() {
        return type;
    }


    public void setTypeParameters(ParameterValueType typeParameters) {
        this.typeParameters = typeParameters == null ? null : typeParameters.getId();
    }

    public ParameterValueType getTypeParameters() {
        return typeParameters == null ? null : ParameterValueType.fromId(typeParameters);
    }


    public void setValInt(Integer valInt) {
        this.valInt = valInt;
    }

    public Integer getValInt() {
        return valInt;
    }

    public void setValString(String valString) {
        this.valString = valString;
    }

    public String getValString() {
        return valString;
    }


}