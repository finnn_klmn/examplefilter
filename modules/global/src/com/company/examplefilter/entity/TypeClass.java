package com.company.examplefilter.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import java.util.List;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@NamePattern("%s|name")
@Table(name = "EXAMPLEFILTER_TYPE_CLASS")
@Entity(name = "examplefilter$TypeClass")
public class TypeClass extends StandardEntity {
    private static final long serialVersionUID = 3849925785091757495L;

    @Column(name = "NAME", nullable = false)
    protected String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID")
    protected TypeClass parent;

    @OneToMany(mappedBy = "parent")
    protected List<TypeClass> children;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "typeClass")
    protected List<TypeClassCharacteristic> characteristic;

    public void setParent(TypeClass parent) {
        this.parent = parent;
    }

    public TypeClass getParent() {
        return parent;
    }

    public void setChildren(List<TypeClass> children) {
        this.children = children;
    }

    public List<TypeClass> getChildren() {
        return children;
    }

    public void setCharacteristic(List<TypeClassCharacteristic> characteristic) {
        this.characteristic = characteristic;
    }

    public List<TypeClassCharacteristic> getCharacteristic() {
        return characteristic;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}