package com.company.examplefilter.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.chile.core.annotations.Composition;
import com.haulmont.cuba.core.entity.annotation.OnDelete;
import com.haulmont.cuba.core.global.DeletePolicy;
import java.util.List;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@NamePattern("%s|name")
@Table(name = "EXAMPLEFILTER_TYPE")
@Entity(name = "examplefilter$Type")
public class Type extends StandardEntity {
    private static final long serialVersionUID = -8349702509846351608L;

    @Column(name = "NAME", nullable = false)
    protected String name;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "type")
    protected List<Typonominal> typonominal;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "TYPE_CLASS_ID")
    protected TypeClass typeClass;

    @Composition
    @OnDelete(DeletePolicy.CASCADE)
    @OneToMany(mappedBy = "type")
    protected List<ParameterValues> parameterValue;

    public void setParameterValue(List<ParameterValues> parameterValue) {
        this.parameterValue = parameterValue;
    }

    public List<ParameterValues> getParameterValue() {
        return parameterValue;
    }


    public void setTyponominal(List<Typonominal> typonominal) {
        this.typonominal = typonominal;
    }

    public List<Typonominal> getTyponominal() {
        return typonominal;
    }

    public void setTypeClass(TypeClass typeClass) {
        this.typeClass = typeClass;
    }

    public TypeClass getTypeClass() {
        return typeClass;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }


}